package com.example.config;

import com.example.annotation.ConditionalOnSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 吕一明
 * @公众号 码客在线
 * @since ${date} ${time}
 */
public class HelloWorldConfig {

    @Bean
    Lv lv () {
        System.out.println("-------------->lv 初始化");
        return new Lv();
    }

}
